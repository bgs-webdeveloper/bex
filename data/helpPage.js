const titles = [
    {
        name: 'Что такое Bex?',
        anchorID: 'about-bex',
    }, {
        name: 'Как обменять валюту?',
        anchorID: 'currency-exchange',
    }, {
        name: 'Оповещения',
        anchorID: 'alerts',
    }, {
        name: 'Техническая поддержка',
        anchorID: 'support'
    }
] 

export const tree = [
    {
      name: titles[0].name,
      link: `#${titles[0].anchorID}`,
    }, {
      name: titles[1].name,
      link: `#${titles[1].anchorID}`,
      list: [
        {
          name: 'Выберите исходную и целевую валюты',
          link: '',
        }, {
          name: 'Рассчитайте итоговую сумму и просмотрите статистику',
          link: '',
        }, {
          name: 'Выберите обменник',
          link: '',
        }, {
          name: 'Перейдите на сайт обменника для конвертации',
          link: '',
        }, {
          name: 'Оставьте отзыв о работе обменника',
          link: '',
        }
      ]
    }, {
      name: titles[2].name,
      link: `#${titles[2].anchorID}`,
    }, {
      name: titles[3].name,
      link: `#${titles[3].anchorID}`,
    }
]

export const aboutSection = {
    anchorID: titles[0].anchorID,
    title: `1. ${titles[0].name}`
}

export const exchangeSection = {
    anchorID: titles[1].anchorID,
    title: `2. ${titles[1].name}`
}

export const alertsSection = {
    anchorID: titles[2].anchorID,
    title: `3. ${titles[2].name}`
}

export const supportSection = {
    anchorID: titles[3].anchorID,
    title: `4. ${titles[3].name}`
}