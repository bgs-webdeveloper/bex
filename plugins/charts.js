import Vue from 'vue';
import ApexChart from 'vue-apexcharts';

Vue.component('VApexChart', ApexChart);