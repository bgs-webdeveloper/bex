
export const logotype = 'https://static.1c.ru/images/guidline/logo_under_1.png'
export const menuLinks = [
    {
        name: 'Мониторинг',
        link: '/'
    }, {  
        name: 'Обменники',
        link: '/exchangers'
    }, {
        name: 'Помощь',
        link: '/help'
    }, {
        name: 'FAQ',
        link: '/faq'
    }, {
        name: 'Контакты',
        link: '/contacts'
    }
]

export const settingsTemplate = {
    fixed: true
}