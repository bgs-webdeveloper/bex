export default (value) => {
    const arr = value.toString().split('').reverse()
    const newArr = []
    arr.forEach((item, i) => {
        if (!(i % 3)) newArr.push(' ')
        newArr.push(item)
    });
    return newArr.reverse().join('').trim()
}