export const table = [
    {
      id: 1,
      rating: 3,
      name: 'NetEx24',
      link: 'https://www.netex24.net/#/ru/?partner=5002',
      statusWork: 1,
      reserve: 591698,
      reviews: {
        negative: 0,
        total: 18898
      },
      wmbl: 10,
      acts: 5,
      pmts: 2
    }, {
      id: 2,
      rating: 0,
      name: 'WestChange',
      link: 'https://www.netex24.net/#/ru/?partner=5002',
      statusWork: 0,
      reserve: 4403587,
      reviews: {
        negative: 5,
        total: 18898
      },
      wmbl: '-',
      acts: 5,
      pmts: 2
    }, {
      id: 3,
      rating: 1,
      name: 'NetEx24',
      link: 'https://www.netex24.net/#/ru/?partner=5002',
      statusWork: 1,
      reserve: 120458,
      reviews: {
        negative: 10,
        total: 18898
      },
      wmbl: 0,
      acts: '-',
      pmts: 0
    }
]