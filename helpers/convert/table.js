import convertMoney from "@/helpers/convert/money"

export const convertIndexTable = (table) => table.map((item, i) => ({
  info: {
    nameColumn: '',
    render: '',
    colWidth: 1,
    component: "VIconInfo",
    title: item.name,
    link: item.link,
    rating: item.rating
  },
  name: {
    nameColumn: 'Обменник',
    link: item.link,
    blank: true,
    render: item.name,
    side: 'left',
    colWidth: 2,
  },
  giveMoney: {
    nameColumn: 'Отдаете',
    render: `${convertMoney(item.giveMoney)}$ <span class="currency">RUB QIWI</span>`,
    side: 'left',
    colWidth: 4
  },
  getMoney: {
    nameColumn: 'Получаете',
    render: `${convertMoney(item.getMoney)}$ <span class="currency">BTC</span>`,
    side: 'left',
    colWidth: 2
  },
  reserve: {
    nameColumn: 'Резерв',
    render: `${item.reserve}`,
    side: 'left',
  },
  reviews: {
    nameColumn: 'Отзывы',
    render: `<div><span ${item.reviews.negative > 0 ? 'class="color-danger"' : '' } >${item.reviews.negative}</span>/<span class="color-success">${item.reviews.total}</span></div>`
  },
}))

export const convertExchangersTable = (table) => table.map((item, i) => ({
  info: {
    nameColumn: '',
    render: '',
    colWidth: 1,
    component: "VIconInfo",
    title: item.name,
    link: item.link,
    rating: item.rating
  },
  name: {
    nameColumn: 'Обменник',
    link: item.link,
    blank: true,
    render: item.name,
    side: 'left',
    colWidth: 2,
  },
  statusWork: {
    nameColumn: 'Статус',
    render: `${item.statusWork ? 'Работает' : 'Недоступен' }`,
    side: 'left',
    colWidth: 2
  },
  reserve: {
    nameColumn: 'Резерзы',
    render: `${convertMoney(item.reserve)}$`,
    side: 'left',
    colWidth: 2
  },
  wmbl: {
    nameColumn: 'WMBL',
    render: `${item.wmbl}`,
    side: 'left',
    colWidth: 1
  },
  acts: {
    nameColumn: 'ACTS',
    render: `${item.acts}`,
    side: 'left',
    colWidth: 1
  },
  pmts: {
    nameColumn: 'PMTS',
    render: `${item.pmts}`,
    side: 'left',
    colWidth: 1
  },
  reviews: {
    nameColumn: 'Отзывы',
    render: `<div><span ${item.reviews.negative > 0 ? 'class="color-danger"' : '' } >${item.reviews.negative}</span>/<span class="color-success">${item.reviews.total}</span></div>`
  },
}))